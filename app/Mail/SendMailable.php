<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $conf;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($conf="", $data=[])
    {
        $this->conf = $conf;
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    /*
     * Example using
    $param = [
        'name'		    => "Sawai Chungsri",
        'login_url'	    => url('/login')
    ];
    $conf = [
        'view'      => 'emails.register-success',
        'subject'   => env('APP_NAME').' - Registration Successfully',
        'to'        => [[
            'email'     => "sawai.chungsri@codeboxx.tech",
            'name'      => "Sawai Chungsri"
        ]],
        'attachments'   => [
            url('/sample.pdf')
        ],
        'binaries'      => [
            'invoice.pdf'   => file_get_contents(url('/test/invoice'))
        ]
    ];
    try {
        Mail::send(new SendMailable($conf, $param));
        return 'Your email has successfully sent.';
    } catch(\Exception $e){
        return 'Cannot sent email, Error : '.$e->getMessage();
    }
    */
    public function build()
    {
        $from = [
            'address'   => ((isset($this->conf['from']['address']))? $this->conf['from']['address']:config('mail.from.address')),
            'name'      => ((isset($this->conf['from']['name']))? $this->conf['from']['name']:config('mail.from.name')),
        ];
        $subject = (isset($this->conf['subject']))? $this->conf['subject']:env('APP_NAME');
        $param = $this->data;
        $email = $this->view($this->conf['view'], $param);
        if(!empty($this->conf['attachments'])){
            foreach($this->conf['attachments'] as $filePath){
                $email->attach($filePath);
            }
        }
        if(!empty($this->conf['binaries'])){
            foreach($this->conf['binaries'] as $filename=>$file){
                $email->attachData($file, $filename, []);
            }
        }
        if(!empty($this->conf['cc'])){
            $email->cc($this->conf['cc']);
        }
        if(!empty($this->conf['bcc'])){
            $email->bcc($this->conf['bcc']);
        }
        if(!empty($this->conf['reply'])){
            $email->replyTo($this->conf['reply']['address'], $this->conf['reply']['name']);
        }
        return $email->from($from['address'], $from['name'])
                    ->to($this->conf['to'])
                    ->subject($subject);
    }
}
