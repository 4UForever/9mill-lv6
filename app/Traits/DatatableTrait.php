<?php

namespace App\Traits;
use DB;

trait DatatableTrait
{
	/*example table configuration
	$this->table = [
		'name'		=> 'users',
		'header'	=> ['ID', 'Name', 'Email', 'Type', 'Created'],
		'column'	=> ['id', 'name', 'email', 'type', 'created_at'],
		'searchable'=> [0, 1, 1, 1, 0],
		'orderable'	=> [1, 1, 1, 1, 1]
	];
	*/
	public function getDataTable($table=[], $input=[], $where=[])
	{
		$columns = array_filter($table['column']);
        $collects = DB::table($table['name']);
        if(!empty($where)){
            foreach($where as $k=>$v){
                $collects->where($k, $v);
            }
        }
        $total = $collects->count();
        if(!empty($input['search']['value'])){
            $collects->where(function($q) use ($input, $table, $columns){
                $loop = 0;
                foreach($table['searchable'] as $k=>$v){
                    if($v==1){
                        if($loop==0){
                            $q->where($columns[$k], 'like', "%{$input['search']['value']}%");
                            $loop = 1;
                        } else {
                            $q->orWhere($columns[$k], 'like', "%{$input['search']['value']}%");
                        }
                    }
                }
            });
        }

        $filter = $collects->count();
        // echo $collects->toSql();
        $arrays = $collects->offset($input['start'])
                    ->limit($input['length'])
                    ->orderBy($columns[$input['order'][0]['column']], $input['order'][0]['dir'])
                    ->get($columns)->toArray();
        $result = json_decode(json_encode($arrays), true);
        // echo "<pre>";print_r($result);echo "</pre>";
        $rows = [];
        foreach($result as $k=>$val){
            $rows[$k] = [];
            foreach($columns as $col){
                $rows[$k][] = $val[$col];
            }
        }
        $data = ['total' => $total, 'filter' => $filter, 'rows' => $rows];
        return $data;
	}
}
