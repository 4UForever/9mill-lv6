<?php

namespace App\Traits;
use Illuminate\Http\Request;
use Image;

trait UploadTrait
{
	/*
	(example URL return)
	http://sample.test/storage/videos/file_example_MP4_480_1_5MG_201811291752.mp4
	(use this command to enable access)
	php artisan storage:link
	*/
	public function uploadFile(Request $request, $filename="", $path="", $newname="") {
		// echo "<pre>";dd($request);echo "</pre>";
		if( !empty($request) && !empty($filename) ){
			if($request->hasFile($filename)){
				$extension = $request->file($filename)->getClientOriginalExtension();
				$fileNameWithExt = $request->file($filename)->getClientOriginalName();
				$fileNameOld = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
				$fileNameToStore = empty($newname)? $fileNameOld.'_'.date('YmdHi').'.'.$extension:$newname;
				$destinationPath = empty($path)? 'public':'public/'.$path;
				$fullPath = $request->file($filename)->storeAs($destinationPath, $fileNameToStore);
				$storageUrl = str_replace("public", "/storage", $fullPath);
				return $storageUrl;
			} else return NULL;
		} else {
			return NULL;
		}
	}

	public function uploadImage(Request $request, $filename="", $path="", $newname="", $maxWidth="") {
		if( !empty($request) && !empty($filename) ){
			if($request->hasFile($filename)){
				$extension = $request->file($filename)->getClientOriginalExtension();
				$fileNameWithExt = $request->file($filename)->getClientOriginalName();
				$fileNameOld = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
				$fileNameToStore = empty($newname)? $fileNameOld.'_'.date('YmdHi').'.'.$extension:$newname;
				$destinationPath = empty($path)? "storage":"storage/".$path;
				$max_width = empty($maxWidth)? 300:$maxWidth;

				$file = $request->file($filename);
				$image = Image::make($file->getRealPath());
                if($image->width()>$max_width){
                    $image->resize($max_width, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $fullPath = $destinationPath."/".$fileNameToStore;
                $image->save($fullPath);
				return "/".$fullPath;
			} else return NULL;
		} else {
			return NULL;
		}
	}
}
