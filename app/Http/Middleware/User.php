<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Auth::check() && Auth::user()->isUser() ) {
            $user = Auth::user();
            $user->last_active = date("Y-m-d H:i:s");
            $user->save();
            return $next($request);
        } else {
            abort(403, 'Unauthorized action.');
        }
    }
}
