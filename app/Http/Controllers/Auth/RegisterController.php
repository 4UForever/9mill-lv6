<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/email/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $input = $data;
        unset($input["_token"]);
        // unset($input["password"]);
        unset($input["repassword"]);

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'type' => User::DEFAULT_TYPE,
            'detail' => json_encode($input, JSON_UNESCAPED_UNICODE)
        ]);

        $now = date("Y-m-d H:i:s");
        $token = md5($input['email'].$now);
        DB::table('password_resets')->updateOrInsert(
            ['email' => $input['email']],
            ['token' => $token, 'created_at' => $now]
        );

        $param = [
            'name'          => $input['name'],
            'verify_url'    => url('/verify')."/".$token
        ];
        $conf = [
            'view'      => 'emails.register',
            'subject'   => 'ยืนยันการสมัครสมาชิก - '.env('APP_NAME'),
            'to'        => [[
                'email'     => $input['email'],
                'name'      => $input['name']
            ]]
        ];
        try {
            Mail::send(new SendMailable($conf, $param));
        } catch (\Exception $e) {
            return redirect()->route('register')->with('status', 'danger')->with('message', $e->getMessage());
        }
        session(['registered_email'=>$input['email']]);
        return $user;
    }

    public function register(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        if($user){
            if(empty($user->email_verified_at)){
                session(['registered_email'=>$user->email]);
                $message = 'This email has registered and waiting for varification. Please check your email for a verification link.<br>
                If you did not receive the email <a href="'.route('verification.resend').'">click here to request new varification link</a>';
            } else {
                $message = 'This email has already taken';
            }
            return redirect()->route('register')
                ->with('status', 'danger')
                ->with('title', 'Failure!')
                ->with('message', $message)->withInput();
        } else {
            $this->validator($request->all())->validate();
            event(new Registered($user = $this->create($request->all())));
            // $this->guard()->login($user);
            return $this->registered($request, $user)?: redirect($this->redirectPath());
        }
    }
}
