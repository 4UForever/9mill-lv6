<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Models\User;
use DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request){
        $input = $request->all();
        $user = User::where('email', $input['email'])->first();

        if($user){
            DB::table('password_resets')->where('email', $input['email'])->orWhere('created_at', '<', date("Y-m-d H:i:s", strtotime("-7 days")))->delete();
            $now = date("Y-m-d H:i:s");
            $token = md5($user->email.$now);
            $arrInsert = [
                'email'         => $user->email,
                'token'         => $token,
                'created_at'    => $now
            ];
            DB::table('password_resets')->insert($arrInsert);
            $arrUser = json_decode($user->detail, true);
            $param = [
                'name'      => $user->name,
                'reset_url' => url('/password/reset/'.$token)
            ];
            $conf = [
                'view'      => 'emails.forgot',
                /*'from'      => [
                    'address'   => 'sawaiwai@hotmail.com',
                    'name'      => 'Admin'
                ],*/
                'subject'   => 'ยืนยันการตั้งรหัสผ่านใหม่ - '.env('APP_NAME'),
                'to'        => [[
                    'email'   => $user->email,
                    'name'      => $user->name
                ]]
            ];
            Mail::send(new SendMailable($conf, $param));
            $alert = ['type' => 'success', 'message' => 'อีเมล์เพื่อยืนยันการตั้งรหัสผ่านใหม่ได้ถูกส่งไปยัง <b>'.$input['email'].'</b> เรียบร้อยแล้ว!'];
            $btn = ['url' => route('login'), 'message' => 'Back to login'];
            return view('auth.passwords.email', compact('alert', 'btn'));
        } else {
            $alert = ['type' => 'failure', 'message' => 'อีเมล์ <b>'.$input['email'].'</b> ไม่มีในระบบ!'];
            $btn = ['url' => route('password.request'), 'message' => 'Back to recover password'];
            return view('auth.passwords.email', compact('alert', 'btn'));
        }
    }
}
