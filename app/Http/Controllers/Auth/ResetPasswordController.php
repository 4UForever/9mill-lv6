<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token=""){
        $reset = DB::table('password_resets')->where('token', $token)->where('created_at', '>', date("Y-m-d H:i:s", strtotime("-7 days")))->first();
        if($reset){
            return view('auth.passwords.reset', compact('token'));
        } else {
            $alert = ['type' => 'failure', 'message' => 'รหัสยืนยันไม่ถูกต้องหรือหมดอายุ!'];
            $btn = ['url' => route('password.request'), 'message' => 'Back to recover password'];
            return view('auth.passwords.reset', compact('alert', 'btn'));
        }
    }

    public function reset(Request $request){
        $reset = DB::table('password_resets')->where('token', $request->token)->where('created_at', '>', date("Y-m-d H:i:s", strtotime("-7 days")))->first();
        if($reset){
            $messages = [
                "password.required" => "Password is required",
                "password.min" => "Password must be at least 6 digits",
                "password_confirmation.required" => "Password confirmation is required",
                "password_confirmation.same" => "Password confirmation must same as password"
            ];
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6|max:30',
                'password_confirmation' => 'required|same:password'
            ], $messages);
            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                //update password
                User::where('email', $reset->email)->update(['password' => Hash::make($request->password)]);
                DB::table('password_resets')->where('token', $request->token)->delete();
                $alert = ['type' => 'success', 'message' => 'ระบบได้ทำการเปลี่ยนแปลงรหัสผ่านของท่านเรียบร้อยแล้ว!'];
                $btn = ['url' => route('login'), 'message' => 'Go to Login'];
                return view('auth.passwords.reset', compact('alert', 'btn'));
            }
        } else {
            $alert = ['type' => 'failure', 'message' => 'รหัสยืนยันไม่ถูกต้องหรือหมดอายุ!'];
            $btn = ['url' => route('login'), 'message' => 'Go to Login'];
            return view('auth.passwords.reset', compact('alert', 'btn'));
        }
    }
}
