<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        /* $this->middleware('signed')->only('verify'); */
        $this->middleware('throttle:2,1')->only('verify', 'resend');//request as max 2 times per minute
    }

    public function show() {
        $registered_email = session('registered_email');
        $message = 'Before proceeding, please check your email for a verification link.<br>If you did not receive the email, <a href="'.route('verification.resend').'">click here to request another</a>';
        return view('auth.verify', compact('registered_email', 'message'));
    }

    public function resend(Request $request)
    {
        $registered_email = session('registered_email');
        $user = User::where('email', $registered_email)->first();
        if($user){
            if(empty($user->email_verified_at)){
                $now = date("Y-m-d H:i:s");
                $token = md5($user->email.$now);
                DB::table('password_resets')->updateOrInsert(
                    ['email' => $user->email],
                    ['token' => $token, 'created_at' => $now]
                );

                $param = [
                    'name'          => $user->name,
                    'verify_url'    => url('/email/verify')."/".$token
                ];
                $conf = [
                    'view'      => 'emails.welcome',
                    'subject'   => 'Register success',
                    'to'        => [[
                        'email'     => $user->email,
                        'name'      => $user->name
                    ]]
                ];
                try {
                    Mail::send(new SendMailable($conf, $param));
                } catch (\Exception $e) {
                    $alert = ['type' => 'danger', 'message' => $e->getMessage()];
                    return view('auth.verify', compact('registered_email', 'alert'));
                }
                $alert = ['type' => 'success', 'message' => 'A fresh verification link has been sent to your email address.'];
                $message = 'Before proceeding, please check your email for a verification link.<br>If you did not receive the email, <a href="'.route('verification.resend').'">click here to request another</a>';
                return view('auth.verify', compact('registered_email', 'alert', 'message'));
            } else {
                return redirect($this->redirectPath());
            }
        } else {
            abort(404, 'Not found registered email.');
        }
        /* if ($request->user()->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }
        $request->user()->sendEmailVerificationNotification();
        return back()->with('resent', true); */
    }

    public function verify(Request $request, $id=""){
        DB::table('password_resets')->where('created_at', '<', date("Y-m-d H:i:s", strtotime("-7 days")))->delete();
        $res = DB::table('password_resets')->where('token', $id)->first();
        if($res){
            $user = User::where('email', $res->email)->first();
            if($user){
                $user->email_verified_at = date("Y-m-d H:i:s");
                $user->save();
            }
            $registered_email = $res->email;
            $alert = ['type' => 'success', 'message' => 'Your email has been verified successfully.'];
            $message = 'Now you can login with your account, <a href="'.route('login').'">click here to login</a>';
            return view('auth.verify', compact('registered_email', 'alert', 'message'));
        } else {
            $registered_email = '';
            $alert = ['type' => 'danger', 'message' => 'Your token mismatch or has been expired.'];
            $message = '<a href="'.route('login').'">Click here to login</a>';
            return view('auth.verify', compact('registered_email', 'alert', 'message'));
        }
    }
}
