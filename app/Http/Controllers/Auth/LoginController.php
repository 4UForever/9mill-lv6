<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        if(auth()->user()->isAdmin()) {
            return '/admin/dashboard';
        } else {
            return '/home';
        }
    }

    public function login(Request $request)
    {
        $messages = [
            "email.required" => "E-mail is required",
            "email.email" => "E-mail address is not valid",
            "email.exists" => "E-mail address is not exists",
            "password.required" => "Password is required"
        ];
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            if (Auth::attempt(['email'=>$request->email, 'password'=>$request->password], $request->remember)) {
                return redirect($this->redirectTo());
            } else {
                return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
                    "password"=>"Password is incorrect",//Set message in case wrong password
                ]);
            }
        }
    }
}
