<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use Image;

class TestController extends Controller
{
	use UploadTrait;

	public function testUploadView() {
		return view('test/test-upload');
	}

    public function testUpload(Request $request) {
    	$input = $request->all();
    	echo "<pre>";print_r($input);echo "</pre>";
    	if(!empty($input['files'])){
    		foreach($input['files'] as $key=>$file){
    			$file->getClientOriginalExtension();
    		}
    	}
    	$files = $request->file();
    	echo "<pre>";print_r($files);echo "</pre>";
	}

	public function testImg(){
		echo "test";
		echo "<br>".public_path('images/abc.jpg');
		$img = Image::make(public_path('images/abc.jpg'));
		$img->resize(300, null, function ($constraint) {
			$constraint->aspectRatio();
		})
		->encode('jpg', 75)
		->save(public_path('images/def.jpg'));
	}

}
