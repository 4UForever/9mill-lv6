<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Routes for utilities
Route::get('/cache-clear', function(){
    \Artisan::call('cache:clear');
    echo 'clear cache complete';
});
Route::get('/storage-link', function(){
    \Artisan::call('storage:link');
    echo 'make storage link complete';
});
Route::get('/db', function(){//used for test DB connection
    try {
        DB::connection()->getPdo();
        if(DB::connection()->getDatabaseName()){
            echo "Successfully connected to the DB: " . DB::connection()->getDatabaseName();
        } else {
            die("Could not find the database. Please check your configuration.");
        }
    } catch (\Exception $e) {
        die("Could not open connection to database server.  Please check your configuration.");
    }
});

//Routes for test
Route::group(['prefix'=>'test'], function(){
    Route::get('/upload-view', 'TestController@testUploadView');
    Route::post('/upload', 'TestController@testUpload');
    Route::get('/img', 'TestController@testImg');
});

// Auth::routes(['verify' => true]);
//use 'php artisan route:list' to view auth route list
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

// Routes for guest
Route::get('/home', 'HomeController@index')->name('home');

//ajax guest
Route::group(['prefix' => 'ajax'], function(){
    Route::get('/csrf', function(){
        return csrf_token();
    });
});

// Routes for authenticated user
Route::group(['middleware' => ['auth', 'user']], function(){
});

// Routes for admin
Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function(){
    Route::get('/dashboard', 'admin\AdminController@dashboard');
});
