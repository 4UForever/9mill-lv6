@extends('layouts.app')

@section('template_title', '404')

@section('template_css')
<link href="//fonts.googleapis.com/css?family=Montserrat:400|Chango" rel="stylesheet">
<style type="text/css">
.page-notfound {
	margin-top: 100px;
}
.page-notfound .notfound {
	display: inline-block;
    padding: 10px;
    border: 7px solid #e01818;
    border-radius: 10px;
}
.page-notfound h1 {
	padding: 0px 40px;
    background: #e01818;
    color: #fff;
    display: inline-block;
    font-family: chango, cursive;
    font-size: 100px;
	margin: 0;
}
.page-notfound h2 {
	font-family: chango, cursive;
    font-size: 68px;
    color: #222;
    font-weight: 400;
    text-transform: uppercase;
    margin: 0;
    line-height: 1.1;
}
.page-notfound .msg {
	font-family: chango, cursive;
    font-size: 30px;
    color: #e01818;
}
</style>
@endsection

@section('content')
<div class="container page-notfound">
    <div class="row pt-5 align-items-center">
		<div class="col-md-5 text-right">
			<div class="notfound"><h1>!</h1></div>
		</div>
		<div class="col-md-7 text-left">
			<h2>Error<br>404</h2>
		</div>
		<div class="col-md-12 text-center mt-4">
			@if(!empty($exception->getMessage()))
			<div class="d-block mb-3 msg">{{ $exception->getMessage() }}</div>
			@endif
			<a href="/" class="btn btn-outline-secondary text-dark font-weight-bold display-1"><span class="fas fa-home fa-2x align-bottom"></span> Back to homepage</a>
		</div>
	</div>
</div>
@endsection
