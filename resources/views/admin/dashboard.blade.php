@extends('layouts.admin')

@section('template_title', 'Dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Overview Users</div>
            <div class="card-body">
                <div class="row mt-2">
                    <div class="col-md-8">Active Users</div><div class="col-md-4 text-right">450</div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8">New Users (30 days)</div><div class="col-md-4 text-right">36</div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8">Canceld Users (active Subscription)</div><div class="col-md-4 text-right">2</div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8">Canceld Users (deactivated)</div><div class="col-md-4 text-right">3</div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-8">Subscription Revenue</div><div class="col-md-4 text-right">64.152 EUR</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
