@extends('emails.template')

@section('header', 'Forgot password')

@section('content')
เรียนคุณ {{ $name }}!<br><br>
ท่านได้ร้องขอการตั้งรหัสผ่านใหม่กับทางเว็บไซต์ {{ env('APP_NAME') }}<br>
กรุณาคลิกลิงค์ด้านล่างเพื่อตั้งรหัสผ่านใหม่
<a href="{{ $reset_url }}" title="ตั้งรหัสผ่านใหม่" style="color:#394b52;">ตั้งรหัสผ่านใหม่</a><br>
<br><br>
@endsection
