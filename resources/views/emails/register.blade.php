@extends('emails.template')

@section('header', 'Register Successfully')

@section('content')
เรียนคุณ {{ $name }}!<br><br>
ท่านได้สมัครสมาชิกกับทางเว็บไซต์ {{ env('APP_NAME') }}<br>
กรุณาคลิกลิงค์ด้านล่างเพื่อยืนยันการสมัครสมาชิก
<a href="{{ $verify_url }}" title="ยืนยันการสมัครสมาชิก" style="color:#394b52;">ยืนยันการสมัครสมาชิก</a><br>
<br><br>
@endsection
