<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta name="format-detection" content="telephone=no">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				@yield('header')
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				@yield('content')
			</div>
		</div>
	</div>
</body>
</html>
