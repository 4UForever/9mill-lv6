<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<style type="text/css">
	.dropzone {
	    width: 100%;
	    height: 200px;
	    border: 2px dashed #77bbcc;
	    text-align: center;
	    border-radius: 10px;
	    background: #fff;
	    display: flex;
	    justify-content: center;
	    align-items: center;
	}
	.dropzone label {
	    color: #337ab7;
	    cursor: pointer;
	}
	.dropzone input[type='file'] {
	    display: none;
	}
</style>
</head>
<body>
	<form id="fEdit" method="post" action="" enctype="multipart/form-data">
		<div class="col-md-12">
			<div class="dropzone">
			    <label for="bu_images">กรุณากดเพื่อเลือกไฟล์ หรือลากไฟล์มาวางที่นี่</label>
			    <input type="file" class="form-control" accept="image/*" name="bu_images[]" id="bu_images" data-limit="10" multiple>
			</div>
		</div>
		<div class="col-md-12 text-center">
			<button class="btn btn-success btn-save" type="button">Save</button>
		</div>
	</form>
	<script type="text/javascript">
		var files;
		$(".dropzone").on('dragover', function (e) {
		    e.preventDefault();
		    e.stopPropagation();
		    $(this).css('border', '#39b311 2px dashed');
			$(this).css('background', '#f1ffef');
		})
		.on("dragleave", function(e) {
		    e.preventDefault();
		    e.stopPropagation();
		})
		.on('drop', function (e) {
		    files = e.originalEvent.dataTransfer.files;
		    return false;
		});
		$(".dropzone input[type='file']").on('change', function(e) {
		    e.preventDefault();
		    files = event.target.files;
		});

		function upload(files){
		    var formData = new FormData();
		    formData.append('_token', "{{ csrf_token() }}");
		    $.each(files, function(key, value) {
		        formData.append('files[]', value);
		    });
		    console.log(files);
		    $.ajax({
		        url: '/test/upload',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        processData: false,
		        contentType: false
		    }).done(function(data){
		        console.log(data);
		    });
		}

		$(".btn-save").click(function(e){
			e.preventDefault();
			upload(files);
		});
		// https://stackoverflow.com/questions/17138244/how-to-display-selected-image-without-sending-data-to-server
		// https://phppot.com/jquery/responsive-image-upload-using-jquery-drag-and-drop/
		// change files to array
		// serialize form
	</script>
</body>
</html>
