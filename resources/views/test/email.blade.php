<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container mt-5">
        @if(!empty($message))
        <div class="alert alert-{{ ($status=="success")? "success":"danger" }} fade show">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $message }}
        </div>
        @endif
        <form action="" method="get">
            <h1>Test send email</h1>
            <div class="row form-group">
                <div class="col-md-12">
                    <label for="email">E-mail</label>
                    <input type="text" class="form-control" name="email" required>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </form>
    </div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</body>
</html>
