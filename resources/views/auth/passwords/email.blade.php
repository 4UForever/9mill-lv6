@extends('layouts.app')

@section('template_title', 'Passwort recover')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (isset($alert))
                    <div class="alert alert-{{ ($alert['type']=='success')? 'success':'danger' }}" role="alert">
                        {!! $alert['message'] !!}
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <a href="{{ $btn['url'] }}" class="btn btn-primary">
                                <span>{{ $btn['message'] }}</span>
                            </a>
                        </div>
                    </div>
                    @else
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    <span>{{ __('Send Password Reset Link') }}</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
