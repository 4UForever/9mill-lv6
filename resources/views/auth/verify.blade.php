@extends('layouts.app')

@section('template_title', 'Verify email')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Email Address') }} {{ " : ".$registered_email }}</div>

                <div class="card-body">
                    @if (isset($alert))
                        <div class="alert alert-{{ $alert['type'] }}" role="alert">
                            {{ $alert['message'] }}
                        </div>
                    @endif

                    {!! $message ?? '' !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
