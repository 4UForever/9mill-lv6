<div class="alert fade show toastStatus" style="position:fixed; top:2%; left:15%; width:70%; display:none; z-index:1;">
    <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <span class="message"></span>
</div>
<script type="text/javascript">
    var myToast;
	$(".toastStatus").on('click', '.close', function(e){
        $(this).parent().hide();
        clearTimeout(myToast);
    });
    function toastStatus(type="", message=""){
        var div = $(".toastStatus");
    	if(type=="success"){
            div.addClass("alert-success").slideDown('slow').find(".message").html(message);
            myToast = setTimeout(()=>{ div.fadeOut('slow').removeClass("alert-success"); }, 5000);
    	} else {
            div.addClass("alert-danger").slideDown('slow').find(".message").html(message);
            myToast = setTimeout(()=>{ div.fadeOut('slow').removeClass("alert-danger"); }, 5000);
    	}
    }
</script>
