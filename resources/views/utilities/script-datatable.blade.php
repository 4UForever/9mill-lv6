<!-- Needs to include "utilities/toast-status" first -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    var datatable = $('.dataTable').DataTable({
        processing: true,
        serverSide: true,
        pageLength: 25,
        language: {
            processing: "<i class='fas fa-spinner fa-spin fa-4x'></i>"
        },
        ajax: {
            url: "{{ $ajax_data_url }}",
            type: "POST",
            beforeSend: function (request) {
                request.setRequestHeader("X-CSRF-TOKEN", token);
            },
            error:function(err){
                console.log(err);
            }
        }
    });
    $('body').on('click', '.ajaxDelete', function(e){
        e.preventDefault();
        $("#confirmDelete").modal("hide");
        var url = $(this).attr("href");
        $.get(url, { _token:token }, function(res){
            // console.log(res);
            if(res.status=="success"){
                $('.dataTable').DataTable().ajax.reload();
                toastStatus("success", res.message);
            } else {
                toastStatus("failure", res.message);
            }
        });
    });
</script>
