<style type="text/css">
small.text-danger::before {
    /*font: normal normal normal 14px/1 FontAwesome;*/
    font-family: "Font Awesome 5 Free";
    font-weight: 900;
    content: "\f06a\00a0";
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $.validator.setDefaults({
        errorElement: "small",
        errorClass: "text-danger d-block",
        errorPlacement: function(error, element) {
            target = element.closest("div");
            target.append(error);
        },
        highlight: function ( element, errorClass, validClass ) {},
        unhighlight: function(element, errorClass, validClass) {
            var name = "#"+$(element).attr("name")+"-error";
            target = $(element).closest("div").find(name);
            target.remove();
        }
    });
</script>
