<div class="modal" id="confirmDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirm Deletion?</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Are you sure want to delete this record?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn btn-outline btn-light" data-dismiss="modal">Cancel</button>
                <a href="" class="btn btn btn-danger btDelete">Confirm</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#confirmDelete').on('show.bs.modal', function (e) {
        var message = $(e.relatedTarget).attr('data-message');
        var title = $(e.relatedTarget).attr('data-title');
        var ajax = $(e.relatedTarget).attr('data-ajax');
        var href = $(e.relatedTarget).attr('href');
        if(message != "") $(this).find('.modal-body').text(message);
        if(title != "") $(this).find('.modal-title').text(title);
        if(ajax) $(this).find('.modal-footer .btDelete').addClass('ajaxDelete');//must include file script-datatable.blade
        $(this).find('.modal-footer .btDelete').attr('href', href);
    });
</script>